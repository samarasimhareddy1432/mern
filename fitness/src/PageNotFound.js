import React from 'react'

function PageNotFound() {
    const margin={
        marginTop: '70px',
      }
  return (
    <div style={margin}>
      <h1>Page Not Found</h1>
    </div>
  )
}

export default PageNotFound
