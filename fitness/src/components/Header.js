import '../App.css';
import './Header.css'
import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, } from 'reactstrap';
import { FaUserMinus } from 'react-icons/fa';
import { NavLink, Route, Routes } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Contact from './Contact'
// import Login from './Login';
import Cart from './Cart'
import fitness from './Assets/images/fitness.png'
// import { FiUserPlus } from "react-icons/fi";

import Sports from './Sports';
import BMICalculator from './BMICalculator'
// import { useSelector } from 'react-redux';



function Header() {
  

  const handleLogout = () => {
    alert('You have been logged out!');
  };  
  const navbarStyle = {
    backgroundColor: '#f48916',
  };

  const NavLinkStyle = {
    fontWeight: 'bold',
    color: 'white',
  };

  const iconStyle = {
    color: 'white',
    fontSize: '25px',
    marginRight: '5px',
    marginBottom: '10px'
  };

  return (
    <>

      <header className='header'>
        <Navbar style={navbarStyle} light expand="md">
          <NavbarBrand href="/home" style={NavLinkStyle} >
            <NavLink to="/home">
              <img
                src={fitness}
                alt="Logo"
                height="30"
                className="d-inline-block align-hrefp"

              />

              <span className='header-pages-words'> Fitness Club</span>
            </NavLink>
          </NavbarBrand>
          <Nav className="mx-auhref" navbar>
            <NavItem>
              <NavLink to="/home" style={NavLinkStyle} className='header-pages-words'>Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/sports" style={NavLinkStyle} className='header-pages-words'>Sports</NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/bmi" style={NavLinkStyle} className='header-pages-words'>Body Mass Index</NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/about" style={NavLinkStyle} className='header-pages-words'>About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/contact" style={NavLinkStyle} className='header-pages-words'>Contact</NavLink>
            </NavItem>
          </Nav>
          <Nav className="ml-auhref" navbar>
            <NavItem>
              <NavLink to="/">
                <FaUserMinus style={iconStyle} onClick={handleLogout} />
                <buthrefn className='login-buthrefn header-pages-words' onClick={handleLogout}>Logout</buthrefn>
              </NavLink>
            </NavItem>
            {/* <NavItem>
              <NavLink to="/cart">
                <h6 className='header-pages-words'><FaShoppingCart style={iconStyle}  />Cart </h6>
              </NavLink>
            </NavItem> */}
          </Nav>
        </Navbar>
      </header>

      <Routes>
        <Route exact path='/home' element={<Home />} ></Route>
        <Route exact path='/about' element={<About />} ></Route>
        <Route exact path='/contact' element={<Contact />} ></Route>
        <Route exact path='/cart' element={<Cart />}></Route>
        <Route exact path='/sports' element={<Sports />}></Route>
        <Route exact path='/BMI' element={<BMICalculator />}></Route>
      </Routes>

    </>
  );
}

export default Header;



























