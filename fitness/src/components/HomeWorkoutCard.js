import React from 'react'
import legsworkout from './Assets/images/legsworkout.jpg'
import bicep from './Assets/images/bicep.jpg'
import chest from './Assets/images/chest.jpg'
import shoulder from './Assets/images/shoulder.jpg'
import back from './Assets/images/back.jpg'
import cardio from './Assets/images/cardio.jpg'
// import { LuDumbbell } from "react-icons/lu";


function HomeWorkoutCard() {
  return (
    <div className='margin'>
      <h1 className='home-card-heading-center'>Daily workouts</h1>
      <div className='homeWorkoutCard-flex margin'>
        <div >
          {/* <!-- Button trigger modal --> */}
          <h1 className='home-card-heading-center'>Monday</h1>
          <div className='home-card-border'>
            <img data-bs-target="#cardioBackdrop" data-bs-toggle="modal" src={cardio} className="img-thumbnail " alt="legs-workout" height={400} width={400} />


            {/* <!-- Modal --> */}
            <div className="modal fade" id="cardioBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="cardioBackdropLabel">Cardio</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <ul>
                      <li>Warm up with a light jog or brisk walk for 10 minutes.</li>
                      <li>Work: run for one minute, giving it an 8 out of 10 effort.</li>
                      <li>Recover: walk or jog for two minutes.</li>
                      <li>Repeat 10 times.</li>
                      <li>Cool down with a light jog or brisk walk for 5 minutes.</li>
                      <li>Stretch to cool down.</li>
                    </ul>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>




        <div>
          {/* <!-- Button trigger modal --> */}
          <h1 className='home-card-heading-center'>Tuesday</h1>
          <div className='home-card-border'>
            <img data-bs-target="#chestBackdrop" data-bs-toggle="modal" src={chest} className="img-thumbnail" alt="legs-workout" height={400} width={400} />l


            {/* <!-- Modal --> */}
            <div className="modal fade" id="chestBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="chestBackdropLabel">Chest workout</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <ul>
                      <li>Incline push up</li>
                      <li>Flat bench press</li>
                      <li>Incline bench press</li>
                      <li>Decline bench press</li>
                      <li>Pushup</li>
                      <li> Cable crossover</li>
                      <li>Chest dip</li>
                      <li>Resistance band pullover</li>
                    </ul>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div>
          {/* <!-- Button trigger modal --> */}
          <h1 className='home-card-heading-center'>Wednesday</h1>
          <div className='home-card-border'>
            <img data-bs-target="#backBackdrop" data-bs-toggle="modal" src={back} className="img-thumbnail" alt="legs-workout" height={400} width={400} />l


            {/* <!-- Modal --> */}
            <div className="modal fade" id="backBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="backBackdropLabel">Back workout</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <ul>
                      <li>Deadlifts</li>
                      <li>Lat Pulldowns</li>
                      <li>Close Grip Lat Pulldown</li>
                      <li>Meadow Rows</li>
                      <li>Supported Rows on incline bench</li>
                      <li>Back Day Burnout Set</li>
                    </ul>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div className='homeWorkoutCard-flex margin'>
        <div>
          {/* <!-- Button trigger modal --> */}
          <h1 className='home-card-heading-center' >Thursday</h1>

          <div className='home-card-border'>
            <img data-bs-target="#bicepBackdrop" data-bs-toggle="modal" src={bicep} className="img-thumbnail" alt="legs-workout" height={400} width={400} />l


            {/* <!-- Modal --> */}
            <div className="modal fade" id="bicepBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="bicepBackdropLabel">Bicep workout</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <ul>
                      <li>DRAG CURL</li>
                      <li>SPIDER CURL</li>
                      <li>BAYESIAN CURL</li>
                      <li>WAITER CURL</li>
                      <li>CHEAT CURLS</li>
                      <li>SITTING DUMBBELL CURLS</li>
                    </ul>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          {/* <!-- Button trigger modal --> */}
          <h1 className='home-card-heading-center'>Friday</h1>
          <div className='home-card-border'>
            <img data-bs-target="#shoulderBackdrop" data-bs-toggle="modal" src={shoulder} className="img-thumbnail" alt="legs-workout" height={400} width={400} />l


            {/* <!-- Modal --> */}
            <div className="modal fade" id="shoulderBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="shoulderBackdropLabel">Legs workout</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <ul>
                      <li>Push Press</li>
                      <li>Barbell Front Shrug</li>
                      <li>Sitting Dumbbell Press</li>
                      <li>Upright Row</li>
                      <li>Cable Lateral Raise</li>
                      <li>Face Pull</li>
                      <li>Barbell Overhead Press</li>
                    </ul>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div>
          {/* <!-- Button trigger modal --> */}
          <h1 className='home-card-heading-center'>Saturday</h1>
          <div className='home-card-border'>
            <img data-bs-target="#legsBackdrop" data-bs-toggle="modal" src={legsworkout} className="img-thumbnail " alt="legs-workout" height={400} width={400} />l


            {/* <!-- Modal --> */}
            <div className="modal fade" id="legsBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="legsBackdropLabel">Legs workout</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <ul>
                     <li>A 10-minute warm-up on the treadmill or elliptical.</li>
                     <li>Bodyweight squats - Three sets of 10 reps.</li>
                     <li>Lunges - Three sets of 10 reps.</li>
                     <li>Leg press - Three sets of 10 reps.</li>
                     <li>Leg curls - Three sets of 10 reps.</li>
                     <li>A five to 10-minute cool-down doing stretches or an ab workout.</li>
                    </ul>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        

      </div>
    </div>
  )
}

export default HomeWorkoutCard
