import React from 'react'
import './Footer.css'
import { FaFacebookSquare, FaYoutube, } from 'react-icons/fa';
import { BsTwitterX } from "react-icons/bs";
import { FaInstagram } from "react-icons/fa";
// import { FaLinkedinIn } from "react-icons/fa6";
import { BiLogoGmail } from "react-icons/bi";
import { Link } from 'react-router-dom';




function Footer() {
  return (
    <div className='footer-background-container'>
      <div className='footer-left-fitnessclub-card-container'>
        <h2>Fitness Club</h2>
        <span>At Fitness club, we make group workouts fun, daily food healthy & tasty, mental fitness easy with yoga & meditatin, Medical & lifestyle care hassle free.</span>
        <h6>#BeBetterEveryDay</h6>
      </div>
      <div className='footer-card-container'>
        <span>Fitness Club for business</span>
        <span>Fitness Club franchise</span>
        <span>corporate patnerships</span>
        <span>Fitness Club pass network</span>
        <span>t&c for business</span>
      </div>
      <div className='footer-card-container'>
        <span>partner.FitnessClub</span>
        <span>blogs</span>
        <span>security</span>
        <span>careers</span>
      </div>
      <div className='footer-card-container'>
        <a href="/contact" target='blank'>contact us</a>
        <span>privacy policy</span>
        <Link to="/bmi">fitness club bmi calculator</Link>
        <span>terms & conditions </span>
      </div>
      <div className='footer-social-media'>
        <h3>Social Media</h3>
        <a className='footer-social-media-icons facebook' href="https://www.facebook.com/" target="blank" ><FaFacebookSquare /></a>
        <a className='footer-social-media-icons youtube' href="https://www.youtube.com/" target="blank"><FaYoutube /></a>
        <a className='footer-social-media-icons twitter' href="https://twitter.com/" target="blank"><BsTwitterX /></a>
        <a className='footer-social-media-icons instagram' href="https://www.instagram.com/" target="blank"><FaInstagram /></a>
        <a className="footer-social-media-icons gmail" href="https://mail.google.com/mail/u/0/#inbox?compose=new"><BiLogoGmail /></a>
      </div>
    </div>
  )
}

export default Footer
