import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { useDispatch } from 'react-redux';
import { IoIosPricetags } from "react-icons/io";


function HomePricePlans() {
    // const dispatch = useDispatch()
    return (

        <div>
            <div className="home-price-plan-programs-header">
                <span className="home-price-plan-stroke-text">Explore our</span>
                <span>Programs</span>
                <span className="home-price-plan-stroke-text">To shape you</span>
            </div>
            <div className='home-price-plan-programs-card-container' >
                <div className='home-price-plan-programs-card' >
                    <span><FontAwesomeIcon icon="fa-solid fa-dumbbell" size="2xl" /></span>
                    <h4>Strength Training</h4>
                    <span>In this program, you are trained to improve your strength through many exercises.</span>
                    <span>Join Now <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
                </div>
                <div className='home-price-plan-programs-card'>
                    <span><FontAwesomeIcon icon="fa-solid fa-person-running" size="2xl" /></span>
                    <h4>Cardio Training</h4>
                    <span>In this program, you are trained to do sequential moves in range of 20 until 30 minutes.</span>
                    <span>Join Now <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
                </div>
                <div className='home-price-plan-programs-card'>
                    <span><FontAwesomeIcon icon="fa-solid fa-fire" size="2xl" /></span>
                    <h4>Fat Burning</h4>
                    <span>This program is suitable for you who wants to get rid of your fat and lose their weight.</span>
                    <span>Join Now <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
                </div>
            </div>
            <div class="home-price-plan-programs-header" >
                <span class="home-price-plan-stroke-text">Ready to Start</span>
                <span>Your Journey</span>
                <span class="home-price-plan-stroke-text">now withus</span>
            </div>
            <div className='home-price-basic-plan-container'>

                <div >
                    {/* plan - 1 */}
                    <div className='home-price-basic-pro-plan'>
                        <span><FontAwesomeIcon icon="fa-solid fa-heart-pulse" size='2xl' /></span>
                        <h3>BASIC PLAN</h3>
                        <h1>₹ 8,000</h1>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />12 Months</span>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Free consultaion to coaches</span>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Access to The Community</span>
                        <span data-bs-toggle="modal" data-bs-target="#modal3">See more benefits  <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
                        <button class="home-price-join-now-button" data-bs-toggle="modal" data-bs-target="#modal1">Join now</button>
                    </div>
                    {/* <!-- Button 1 --> */}
                    <div class="modal fade" id="modal1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal1Label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modal1Label">Basic Plan</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                {/* button - 1 */}
                                <div class="modal-body ">
                                    <div className='home-price-tag-container'>
                                        <div>
                                            <h4>12 Months</h4>
                                            <input type="date" />
                                        </div>
                                        <div className='home-price-tag'>
                                            <span className='strike'>price ₹ 17,550</span>
                                            <span className='home-price-pop'>After% ₹ 8,190</span>
                                            <small className='home-price-pop'>GST   ₹ +1,474</small> <hr />
                                            <small >Total ₹ 9664 </small>
                                        </div>
                                    </div>
                                    <h3>Offer</h3>
                                    <p><IoIosPricetags /> Get FREE 1.5 months extension  </p>
                                    <p><IoIosPricetags />Only Today I Free Amazon voucher worth ₹500  </p>
                                    <p> <IoIosPricetags /> Only Today I Additional ₹1500 off applied.  </p>
                                    <p><IoIosPricetags /> 10 Days of membership pause  </p>
                                    <h3>About this pack</h3>
                                    <p>Book unlimited Classes anytime at any centre  in your city for 12 months
                                        Every fitness centre offers a plethora of group workout formats designed and run by highly qualified fitness experts.
                                        These workouts are great for newbies and fitness veterans alike, and are guaranteed to show results.
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                    <a href="https://paytm.com/  " target='blank'><button type="button" class="btn btn-primary">Pay</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div >
                    {/* plan - 2 */}
                    <div className='home-price-premium-plan'>
                        <span><FontAwesomeIcon icon="fa-solid fa-crown" size='2xl' /></span>
                        <h3>PREMIUM PLAN</h3>
                        <h1>₹ 13,490</h1>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />36 Months</span>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Free consultaion to coaches</span>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Access to steam bath</span>
                        <span data-bs-toggle="modal" data-bs-target="#modal3">See more benefits  <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
                        <button class="home-price-join-now-button" data-bs-toggle="modal" data-bs-target="#modal2">Join now</button>
                    </div>
                    {/* <!-- Button 2 --> */}
                    <div class="modal fade" id="modal2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal2Label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modal2Label">PREMIUM PLAN</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div className='home-price-tag-container'>
                                        <div>
                                            <h4>36 Months</h4>
                                            <input type="date" />
                                        </div>
                                        <div className='home-price-tag'>
                                            <span className='strike'>price ₹ 45,550</span>
                                            <span className='home-price-pop'>After% ₹ 13,490</span>
                                            <small className='home-price-pop'>GST   ₹ +2,428</small> <hr />
                                            <small >Total ₹ 15,918 </small>
                                        </div>
                                    </div>
                                    <h3>Offer</h3>
                                    <p><IoIosPricetags /> Get FREE 3 months extension  </p>
                                    <p><IoIosPricetags />Only Today I Free Amazon voucher worth ₹1000  </p>
                                    <p> <IoIosPricetags />Only Today I Additional ₹1500 off applied.  </p>
                                    <p><IoIosPricetags /> 45 Days of membership pause  </p>
                                    <h3>About this pack</h3>
                                    <p>Book unlimited Classes anytime at any centre in your city for 36 months.
                                        Every fitness centre offers a plethora of group workout formats
                                        designed and run by highly qualified fitness experts.
                                        These workouts are great for newbies and fitness veterans alike, and
                                        are guaranteed to show results.
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                    <a href="https://paytm.com/  " target='blank'><button type="button" class="btn btn-primary">Pay</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div >
                    {/* plan - 3 */}
                    <div className='home-price-basic-pro-plan'>
                        <span><FontAwesomeIcon icon="fa-solid fa-dumbbell" size="2xl" /></span>
                        <h3>PRO PLAN</h3>
                        <h1>₹ 11,090</h1>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />24 Months</span>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' /> consultaion of Private Coaches</span>
                        <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Free Fitness Merchandises</span>
                        <span data-bs-toggle="modal" data-bs-target="#modal3">See more benefits  <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
                        <button class="home-price-join-now-button" data-bs-toggle="modal" data-bs-target="#modal3">Join now</button>

                    </div>
                    {/* <!-- Button 3 --> */}

                    {/* <!-- Modal 3 --> */}
                    <div class="modal fade" id="modal3" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal3Label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modal3Label">PRO PLAN</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div className='home-price-tag-container'>
                                        <div>
                                            <h4>24 Months</h4>
                                            <input type="date" />
                                        </div>
                                        <div className='home-price-tag'>
                                            <span className='strike'>price ₹ 23,550</span>
                                            <span className='home-price-pop'>After% ₹ 11,090</span>
                                            <small className='home-price-pop'>GST   ₹ +1,996</small> <hr />
                                            <small >Total ₹ 12,996 </small>
                                        </div>
                                    </div>
                                    <h3>Offer</h3>
                                    <p><IoIosPricetags /> Get FREE 2.5 months extension  </p>
                                    <p><IoIosPricetags />Only Today I Free Amazon voucher worth ₹750  </p>
                                    <p> <IoIosPricetags /> Only Today I Additional ₹1500 off applied.  </p>
                                    <p><IoIosPricetags /> 25 Days of membership pause  </p>
                                    <h3>About this pack</h3>
                                    <p>
                                        Book unlimited Classes anytime at any centre  in your city for 6 months.
                                        Every club fitness centre offers a plethora of group workout formats designed and run
                                        by highly qualified fitness experts. These workouts are great for newbies
                                        and fitness veterans alike, and are guaranteed to show results.
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                    <a href="https://paytm.com/  " target='blank'><button type="button" class="btn btn-primary">Pay</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    )
}

export default HomePricePlans
